FROM java:8

WORKDIR /

ADD target/opendatamaintenance-0.0.1-SNAPSHOT.jar opendatamaintenance.jar

EXPOSE 8080

CMD java -jar opendatamaintenance.jar


