TAG_VERSION="$1"
DEPLOYMENT_FILE_PATH="$2"


if [ "$2" == "" ]; then
    echo "Missing parameter! Parameters are TAG_VERSION and DEPLOYMENT_FILE_PATH.";
    exit 1;
fi

cat > ${DEPLOYMENT_FILE_PATH} <<EOL
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: opendata
spec:
  replicas: 2
  template:
    metadata:
      labels:
        app: opendata
    spec:
      containers:
        - name: opendata
          image: registry.gitlab.com/mspr-maintenance-et-evolution-de-l-application/api:$TAG_VERSION
          imagePullPolicy: Always
          ports:
            - containerPort: 8080
      imagePullSecrets:
        - name: registry.gitlab.com
EOL
