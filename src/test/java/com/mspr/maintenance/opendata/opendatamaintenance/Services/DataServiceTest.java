package com.mspr.maintenance.opendata.opendatamaintenance.Services;

import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import com.mspr.maintenance.opendata.opendatamaintenance.Repository.DataRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@WebMvcTest(DataService.class)
class DataServiceTest {

    @MockBean
    private DataRepository dataRepository;
    @Autowired
    private DataService dataService;
    private String csvString="1;parc;montcalme\n2;évènement;estivales";

    private Collection<Data> datasfile;
    @BeforeEach
    void setUp() {
        datasfile=new ArrayList<>();
        datasfile.add(new Data(1,"parc","montcalme"));
        datasfile.add(new Data(2,"évènement","estivales"));
        datasfile.add(new Data(3,"hotpital","lapeyronie"));
    }

    @Test
    void saveFileData() throws FileException {
        MultipartFile file=new MockMultipartFile("file","orig","text/csv",csvString.getBytes());
        String csv = dataService.saveFileData(file, "csv");
        assertEquals("Fichier sauvegardé",csv);
    }

    @Test
    void getAll() {
        when(dataRepository.findAll()).thenReturn(datasfile);
        Collection<Data> datas=dataService.getAll();
        assertEquals(datas.size(),datasfile.size());
    }

    @Test
    void saveFilesData() throws FileException {
        MultipartFile file=new MockMultipartFile("file","orig","text/csv",csvString.getBytes());
        MultipartFile[] files=new MultipartFile[1];
        files[0]=file;
        HashMap<String, String> csv = dataService.saveFilesData(files, "csv");
        Set<String> keys = csv.keySet();
        assertEquals("Fichier sauvegardé",csv.get("file"));
    }
}