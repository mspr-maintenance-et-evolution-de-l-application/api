package com.mspr.maintenance.opendata.opendatamaintenance.FileReader;

import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class FileReaderCSVTest {

    private FileReaderCSV fileReaderCSV;
    private Collection<Data> datas;
    private String csvString="1;parc;montcalme\n2;évènement;estivales";
    private MultipartFile file;

    @BeforeEach
    void setUp() {
        datas=new ArrayList<>();
        datas.add(new Data(1,"parc","montcalme"));
        datas.add(new Data(2,"évènement","estivales"));
        file=new MockMultipartFile("file","orig","text/csv",csvString.getBytes());
        fileReaderCSV=new FileReaderCSV(file);
    }

    @Test
    void extractData() throws FileException {
        Collection<Data> dataRead = fileReaderCSV.extractData();
        assertEquals(dataRead.size(),datas.size());
    }
}