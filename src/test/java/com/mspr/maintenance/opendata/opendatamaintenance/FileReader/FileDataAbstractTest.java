package com.mspr.maintenance.opendata.opendatamaintenance.FileReader;

import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class FileDataAbstractTest {

    private Collection<Data> datas;
    private String csvString="1;parc;montcalme\n2;évènement;estivales";
    private MultipartFile file;
    private FileData fileData;

    @BeforeEach
    void setUp() {
        datas=new ArrayList<>();
        datas.add(new Data(1,"parc","montcalme"));
        datas.add(new Data(2,"evenement","estivales"));
        file=new MockMultipartFile("file","orig","text/csv",csvString.getBytes());
        fileData=new FileData(file,"csv");
    }

    @Test
    void getFile() {
        MultipartFile fileGet = fileData.getFile();
        assertEquals(fileGet.getName(),file.getName());
    }

    @Test
    void setFile() {
        MockMultipartFile fileN = new MockMultipartFile("newFile", "orig", "text/csv", csvString.getBytes());
        fileData.setFile(fileN);
        assertEquals(fileN.getName(),fileData.getFile().getName());
    }

    @Test
    void getType() {
        String type=fileData.getType();
        assertEquals(type,"csv");
    }

    @Test
    void setType() {
        String ntype="jpeg";
        fileData.setType(ntype);
        assertEquals(fileData.getType(),ntype);
    }

    @Test
    void getContent() throws FileException {
        String content = fileData.getContent();
        assertEquals(content,csvString);
    }

    public class FileData extends FileDataAbstract {


        public FileData(MultipartFile f, String format) {
            super(f, format);
        }

        @Override
        public Collection<Data> extractData() throws FileException {
            return datas;
        }
    }
}