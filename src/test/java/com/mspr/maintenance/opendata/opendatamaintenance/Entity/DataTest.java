package com.mspr.maintenance.opendata.opendatamaintenance.Entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DataTest {

    private Data data;

    @BeforeEach
    void setUp() {
        data=new Data(1,"parc","montcalme");
        data.setId(1);
    }

    @Test
    void getId() {
        long id=data.getId();
        assertEquals(id,1);
    }

    @Test
    void setId() {
        data.setId(2);
        assertEquals(data.getId(),2);
    }

    @Test
    void getName() {
        String name=data.getName();
        assertEquals(name,"parc");
    }

    @Test
    void setName() {
        data.setName("évènement");
        assertEquals(data.getName(),"évènement");
    }

    @Test
    void getValue() {
        String value=data.getValue();
        assertEquals(value,"montcalme");
    }

    @Test
    void setValue() {
        data.setValue("estivales");
        assertEquals(data.getValue(),"estivales");
    }

    @Test
    void getUniqueId() {
        assertEquals(data.getUniqueId(),1);
    }

    @Test
    void setUniqueId() {
        data.setUniqueId(2);
        assertEquals(data.getUniqueId(),2);
    }
}