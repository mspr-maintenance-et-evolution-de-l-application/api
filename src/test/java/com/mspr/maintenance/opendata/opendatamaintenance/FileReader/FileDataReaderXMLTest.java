package com.mspr.maintenance.opendata.opendatamaintenance.FileReader;

import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class FileDataReaderXMLTest {

    private FileDataReaderXML fileDataReaderXML;
    private Collection<Data> datas;
    private String csvString="<import>\n" +
            "\t<data>\n" +
            "\t\t<uniqueID>8000</uniqueID>\n" +
            "\t\t<name>parc</name>\n" +
            "\t\t<value>Montcalme</value>\n" +
            "\t</data>\n" +
            "\t<data>\n" +
            "\t\t<uniqueID>42</uniqueID>\n" +
            "\t\t<name>velomag</name>\n" +
            "\t\t<value>Montcalme</value>\n" +
            "\t</data>\n" +
            "\t<data>\n" +
            "\t\t<uniqueID>41</uniqueID>\n" +
            "\t\t<name>evenement</name>\n" +
            "\t\t<value>Estivales</value>\n" +
            "\t</data>\n" +
            "</import>";

    private MultipartFile file;

    @BeforeEach
    void setUp() {
        file=new MockMultipartFile("file","orig","text/csv",csvString.getBytes());
        fileDataReaderXML=new FileDataReaderXML(file);
    }

    @Test
    void extractData() throws FileException {
        Collection<Data> data = fileDataReaderXML.extractData();
        assertEquals(data.size(),3);
    }
}