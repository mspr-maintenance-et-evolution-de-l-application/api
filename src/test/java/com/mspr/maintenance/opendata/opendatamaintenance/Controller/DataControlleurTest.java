package com.mspr.maintenance.opendata.opendatamaintenance.Controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Services.DataService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DataControlleur.class)
class DataControlleurTest {

    @Autowired
    protected MockMvc mvc;

    @MockBean
    private DataService dataService;

    private MockMultipartFile  file;
    private String csvString="1;parc;montcalme\n2;évènement;estivales";
    private Collection<Data> datas;

    @BeforeEach
    void setUp() {
        datas=new ArrayList<>();
        datas.add(new Data(1,"parc","montcalme"));
        datas.add(new Data(2,"évènement","estivales"));
        file=new MockMultipartFile("file","orig","text/csv",csvString.getBytes());
    }

    @Test
    void uploadFile() throws Exception {
        String answer = "{'message':'Fichier sauvegardé'}";
        when(dataService.saveFileData(file,"csv")).thenReturn("Fichier sauvegardé");
        MvcResult mvcResult = mvc.perform(multipart("/dataset/upload").file(file).param("format","csv").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String rep = mvcResult.getResponse().getContentAsString();
        assertEquals(rep,answer);
    }


    @Test
    void uploadFiles() throws Exception {
        String answer = "{'message':'Fichier sauvegardé'}";
        when(dataService.saveFileData(file,"csv")).thenReturn("Fichier sauvegardé");
        MultipartFile[] files=new MultipartFile[1];
        files[0]=file;
        MvcResult mvcResult = mvc.perform(multipart("/dataset/upload").file(file).param("format","csv").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
        String rep = mvcResult.getResponse().getContentAsString();
        assertEquals(rep,answer);
    }

    @Test
    void getAllData() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        when(dataService.getAll()).thenReturn(datas);
        MvcResult mvcResult = mvc.perform(get("/dataset")).andExpect(status().isOk()).andReturn();
        String contentAsString = mvcResult.getResponse().getContentAsString();
        Collection<Data> dataCollection = objectMapper.readValue(contentAsString, new TypeReference<Collection<Data>>() {});
        assertEquals(datas.size(),dataCollection.size());
    }

}