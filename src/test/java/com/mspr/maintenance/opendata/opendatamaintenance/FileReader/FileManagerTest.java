package com.mspr.maintenance.opendata.opendatamaintenance.FileReader;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import static org.junit.jupiter.api.Assertions.*;

class FileManagerTest {

    private MultipartFile file;
    private String csvString="1;parc;montcalme\n2;évènement;estivales";


    @BeforeEach
    void setUp() {
        file=new MockMultipartFile("file","orig","text/csv",csvString.getBytes());
    }

    @Test
    void getFileReader() {
        FileDataReader csv = FileManager.getFileReader(file, "csv");
        assertEquals(csv.getClass(),FileReaderCSV.class);
    }

    @Test
    void getFileReaderFormatNotFound() {
        FileDataReader csv = FileManager.getFileReader(file, "jpeg");
        assertNull(csv);
    }
}