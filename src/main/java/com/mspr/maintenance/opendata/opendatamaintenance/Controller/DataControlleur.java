package com.mspr.maintenance.opendata.opendatamaintenance.Controller;

import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import com.mspr.maintenance.opendata.opendatamaintenance.Services.DataService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.HashMap;

@RestController
@CrossOrigin("*")
@RequestMapping("/dataset")
public class DataControlleur {

    @Autowired
    private DataService dataService;

    private Logger log=Logger.getLogger(DataControlleur.class);

    /**
     * @param file Fichier contenant les données à sauvegardé
     * @param format format du fichier
     * @return retourne le résultat de la sauvegarde (réussi ou échec)
     * @throws FileException
     */
    @PostMapping(value = "/upload",produces = "application/json;charset=UTF-8")
    public String uploadFile( @RequestParam MultipartFile file, @RequestParam String format) throws FileException {
        log.info("upload file : "+format);
        String message=dataService.saveFileData(file,format);
        return "{'message':'"+message+"'}";
    }

    /**
     * @param files Fichier contenant les données à sauvegardé
     * @param format format du fichier
     * @return retourne le résultat de la sauvegarde (réussi ou échec)
     * @throws FileException
     */
    @PostMapping(value = "/uploads",produces = "application/json;charset=UTF-8")
    public HashMap<String, String> uploadFiles(@RequestParam MultipartFile[] files,  @RequestParam String format) throws FileException {
        log.info("upload file : "+format);
        HashMap<String, String> stringStringHashMap = dataService.saveFilesData(files, format);
        return stringStringHashMap;
    }

    /**
     * @return les données de la base de données
     */
    @GetMapping(value = "",produces = "application/json")
    public Collection<Data> getAllData(){
        log.info("get all datas");
        return dataService.getAll();
    }
}
