package com.mspr.maintenance.opendata.opendatamaintenance.Exception;

public class FileException extends Exception {

    public FileException(){
        super("Fichier non compatible");
    }
}
