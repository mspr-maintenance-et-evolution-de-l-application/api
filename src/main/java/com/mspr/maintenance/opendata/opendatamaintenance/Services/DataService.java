package com.mspr.maintenance.opendata.opendatamaintenance.Services;

import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import com.mspr.maintenance.opendata.opendatamaintenance.FileReader.FileDataReader;
import com.mspr.maintenance.opendata.opendatamaintenance.FileReader.FileManager;
import com.mspr.maintenance.opendata.opendatamaintenance.Repository.DataRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

@Service
public class DataService {

    @Autowired
    private DataRepository dataRepository;

    private Logger log=Logger.getLogger(DataService.class);

    /**
     * @param file fichier contenant les données
     * @param format format du fichier
     * @return retourne le résultat de la sauvegarde des données
     * @throws FileException exception fichier non présent
     */
    public String saveFileData(MultipartFile file, String format) throws FileException {
        String message="Fichier sauvegardé";
        log.info(format);
        FileDataReader filereader= FileManager.getFileReader(file,format);
        if(file==null){
            return "fichier non pris en charge";
        }
        Collection<Data> datas=filereader.extractData();
        dataRepository.saveAll(datas);
        return message;
    }

    /**
     * @return retourne toutes les données
     */
    public Collection<Data> getAll(){
        Collection<Data> datas=new ArrayList<>((Collection<? extends Data>)dataRepository.findAll());
        return datas;
    }

    public HashMap<String,String> saveFilesData(MultipartFile[] files, String format) throws FileException {
        HashMap<String,String> resultat=new HashMap<>();
        for (MultipartFile f:files) {
            String s = saveFileData(f, format);
            resultat.put(f.getName(),s);
        }
        return resultat;
    }
}
