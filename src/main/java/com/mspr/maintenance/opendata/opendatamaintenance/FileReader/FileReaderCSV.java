package com.mspr.maintenance.opendata.opendatamaintenance.FileReader;

import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class FileReaderCSV extends FileDataAbstract{

    private Logger log=Logger.getLogger(FileReaderCSV.class);

    public FileReaderCSV(MultipartFile f) {
        super(f, "csv");
    }

    @Override
    public Collection<Data> extractData() throws FileException {
        String content=getContent();
        log.info(content);
        if(!content.contains(";")){
            throw new FileException();
        }
        Collection<String> lines= Arrays.asList(content.split("\n"));
        Collection<Data> datas=new ArrayList<>();
        for (String line : lines){
            String[]row=line.split(";");
            Data data = getData(row);
            datas.add(data);
        }
        return datas;
    }

    private Data getData(String[] row) {
        long id= Long.parseLong(row[0]);
        String name=row[1];
        String value=row[2];
        return new Data(id,name,value);
    }
}
