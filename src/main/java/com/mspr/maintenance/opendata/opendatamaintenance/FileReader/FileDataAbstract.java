package com.mspr.maintenance.opendata.opendatamaintenance.FileReader;

import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Classe de base permettant d'extraire les données d'un fichier
 */
public abstract class FileDataAbstract  implements FileDataReader{

    private MultipartFile file;
    private String type;
    private Logger log=Logger.getLogger(FileDataAbstract.class);

    /**
     * @param f fichier contenant les données
     * @param format format du fichier
     */
    public FileDataAbstract(MultipartFile f,String format){
        type=format;
        file=f;
    }

    public String getContent() throws FileException {
        byte[] bytes = new byte[0];
        try {
            bytes = getFile().getBytes();
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileException();
        }
        String content=new String(bytes);
        return content;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Logger getLog() {
        return log;
    }
}
