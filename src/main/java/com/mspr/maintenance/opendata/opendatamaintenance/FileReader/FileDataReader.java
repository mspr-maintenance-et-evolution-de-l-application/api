package com.mspr.maintenance.opendata.opendatamaintenance.FileReader;

import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;

public interface FileDataReader {

    /**
     * @return les données du fichier
     * @throws FileException si le format du fichier n'est pas pris en charge ou incorrect
     */
    public Collection<Data> extractData() throws FileException;
}
