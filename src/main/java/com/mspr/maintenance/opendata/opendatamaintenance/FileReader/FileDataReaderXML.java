package com.mspr.maintenance.opendata.opendatamaintenance.FileReader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import com.mspr.maintenance.opendata.opendatamaintenance.Exception.FileException;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FileDataReaderXML extends FileDataAbstract{
    /**
     * @param f      fichier contenant les données
     */
    public FileDataReaderXML(MultipartFile f) {
        super(f, "xml");
    }

    @Override
    public Collection<Data> extractData() throws FileException {
        String content=getContent().substring("<import>".length(),getContent().length()-"</import>".length());
        String[]datas=content.split("<data>");
        Collection<Data> datasObjects=new ArrayList<>();
        for (String data:datas) {
            data=data.trim();
            if(!data.isEmpty()){
                String[]strSplit=data.split("</uniqueID>");
                int uniqueID=Integer.parseInt(strSplit[0].trim().substring("<uniqueID>".length()));
                strSplit=strSplit[1].trim().split("</name>");
                String name=strSplit[0].trim().substring("<name>".length());
                strSplit=strSplit[1].split("</value>");
                String value=strSplit[0].trim().substring("<value>".length());
                Data dataObject=new Data(uniqueID,name,value);
                datasObjects.add(dataObject);
            }
        }
        return datasObjects;
    }
}
