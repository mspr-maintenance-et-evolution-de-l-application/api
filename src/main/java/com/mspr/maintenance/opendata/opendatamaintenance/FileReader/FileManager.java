package com.mspr.maintenance.opendata.opendatamaintenance.FileReader;

import org.springframework.web.multipart.MultipartFile;

public class FileManager {

    /**
     * @param file Fichier contenant les données
     * @param format format du fichier
     * @return retourne le FileReader correspondant au fichier ou null si le type de fichier n'est pas pris en charge
     */
    public static FileDataReader getFileReader(MultipartFile file,String format){
        switch (format){
            case "csv":
                return new FileReaderCSV(file);
            case "xml":
                return new FileDataReaderXML(file);
            default:
                return null;
        }
    }
}
