package com.mspr.maintenance.opendata.opendatamaintenance.Entity;

import org.springframework.web.bind.annotation.CrossOrigin;

import javax.persistence.*;

/**
 * Data données
 */
@Entity
@Table(name = "data")
public class Data {

    /**
     * Identifiant unique des données
     */
    private long id;

    /**
     * identifiant unique item
     */
    private long uniqueId;
    /**
     * Nom de la donnée
     */
    private String name;
    /**
     * Valeur de la donnée
     */
    private String value;

    public Data(){}

    public Data(long id,String n,String v){
        this.uniqueId=id;
        name=n;
        value=v;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id",nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name="value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "uniqueId",nullable = false,unique = true)
    public long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(long uniqueId) {
        this.uniqueId = uniqueId;
    }
}
