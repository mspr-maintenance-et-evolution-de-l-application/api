package com.mspr.maintenance.opendata.opendatamaintenance.Repository;

import com.mspr.maintenance.opendata.opendatamaintenance.Entity.Data;
import org.springframework.data.repository.CrudRepository;

public interface DataRepository extends CrudRepository<Data,Long> {
}
